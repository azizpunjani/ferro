<?php
/**
 * Router class used to route request to destination controller/action
 */
class router
{
    public  static $defaultRoute = array();
    public  static $controller; 
    public  static $action; 

    public static function routeRequest()
    {
        $arguments_array = array();

        #-- Explode over ? to seperate the query string ( if one is present ) 
        #-- e.g /home/index?firstKey=firstVal&secondKey=secondVal
        #-- would result in Array( [0] => '/home/index', [1] => 'firstVal&secondKey=secondVal' )
        #-- A url without a query string would result in an array with the url in index 0 
        $url_segments = explode( '?', $_SERVER['REQUEST_URI'] );

        #-- Destination url will be the first array 
        $destination  = $url_segments[0];  

        #-- Get destination segments
        #-- e.g /home/index would result in Array( [0] => '', [1] => home, [2] => index 
        $destination_segments = explode( '/', $destination );

        #-- Url decode the segments incase of a space 
        $destination_segments = array_map(function( $segment ){ 
            return trim( urldecode( $segment )); 				
        }, $destination_segments); 		

        #-- Destination segments index [1] should contain the controller name 
        if( isset( $destination_segments[1] ) && !utility::isEmptyString( $destination_segments[1] )  )
        {
            self::$controller = $destination_segments[1]; 
        }
        else 
        {  
            if( isset( self::$defaultRoute['controller'] ) )
            {
                self::$controller = self::$defaultRoute['controller'];
            }  
            else 
            {
                throw new RouteNotFoundException('Unable to determine destination controller'); 
            }
        }

        #-- Destination segments index [2] should contain the action, if not default it
        if( isset( $destination_segments[2] ) && !utility::isEmptyString( $destination_segments[2] ) )
        {
            self::$action = $destination_segments[2];  
        }
        else 
        {
            if( isset( self::$defaultRoute['action'] ) )
            {
                self::$action = self::$defaultRoute['action'];
            }
            else 
            {
                throw new RouteNotFoundException('Unable to determine destination action');   
            }
        }


        $controller_file_path = framework::getControllersPath().self::$controller.framework::getFileExtension();

        #-- Anything after segments 2 are arguments to the action
        if( count( $destination_segments ) > 3 )
        {
            $arguments_array = array_splice( $destination_segments, 3 );
        }

        #-- Check if the controller exists  
        if( file_exists( $controller_file_path ) )
        {
            require_once( $controller_file_path );

            #-- Instantiate controller 
            $controller = new self::$controller(); 
            $action = self::$action; 

            #-- Check to see if the action method exists 
            if( method_exists( $controller, $action ) )
            { 
                #-- Call the controller action and pass in the arguments 	
                call_user_func_array( array( $controller, $action ), $arguments_array );
                exit; 
            } 
        }
        else 
        {
            throw new RouteNotFoundException('Unable to route request to '.$controller_file_path);
        }

    }

    /**
     * Set default controller => action 
     * @param array - $route array( 'controller => 'controller name', 'action' => 'action method' )
     */
    public static function setDefaultRoute( $route = array() )
    {
        self::$defaultRoute = $route; 
    }
}

class RouteNotFoundException extends Exception{}
