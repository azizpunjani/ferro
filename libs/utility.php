<?php
/**
 * Utility class for misc helper functions 
 */
class utility
{
    /**
     * Check if string is empty
     * @param string $string
     * @return boolean
     */
    public static function isEmptyString( $string )
    {
        return strlen( trim( $string ) ) === 0;  
    }

    /**
     * Prints a variable/object/array in a redable format
     * for debug purposes only. 
     */
    public static function printFriendly( $variable )
    {
        echo '<pre>'; 
        print_r( $variable ); 
        echo '</pre>'; 
    }
}
