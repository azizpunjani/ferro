<?php
/**
 * Core framework class. 
 * This class registers the autoloader function used throughtout the framework, 
 * as well as sets up global variables for easy access in the framework. 
 */
class framework
{
    private static $application_path;
    private static $config; 
    private static $controllers_path; 
    private static $file_extension; 
    private static $library_path; 
    private static $models_path;
    private static $system_path;
    private static $system_library_path; 
    private static $views_path; 

    /**
     * Initializes framework 
     * 1. Sets config vars 
     * 2. Registers autoloader function 
     * 3. Sets the framework paths 
     */
    public static function initialize()
    {
        #-- Set paths for ease of access the framework
        self::setPaths();

        #-- Setup the config so it's available throughout the framework 
        self::setConfigVars();

        #-- Register the autoloader function
        self::registerAutoloader();

        #-- Set default script extension 
        self::setFileExtension(); 
    }

    /**
     * Getter for application path
     * @return string
     */
    public static function getApplicationPath()
    {
        return self::$application_path;
    }

    /**
     * Getter for config array  
     * @return array
     */
    public static function getConfig()
    {
        return self::$config; 
    }

    /**
     * Getter for controller path 
     * @return string
     */
    public static function getControllersPath()
    {
        return self::$controllers_path; 
    }

    /**
     * Get the default file extension
     */
    public static function getFileExtension()
    {
        return self::$file_extension;
    }

    /**
     * Getter for library path
     * @return string
     */
    public static function getLibraryPath()
    {
        return self::$library_path;
    }

    /**
     * Getter for models path
     * @return string
     */
    public static function getModelsPath()
    {
        return self::$models_path;
    }

    /**
     * Getter for the systems library path
     * @return string 
     */
    public static function getSystemLibraryPath()
    {
        return self::$system_library_path; 
    }

    /**
     * Getter for views path 
     * @return string
     */
    public static function getViewsPath()
    {
        return self::$views_path; 
    }


    /**
     * Register the autoloader function. This function will check the following paths 
     * 1. The application controller folder. 
     * 2. The views path  
     */
    private static function registerAutoloader()
    {
        spl_autoload_register(function( $file ){
            $extension = FILE_EXT; 
            $paths = array(
                'controllers_path'    => framework::getControllersPath(),
                'models_path' 	      => framework::getModelsPath(),
                'system_library_path' => framework::getSystemLibraryPath()
            );

            foreach( $paths as $name => $path )
            {	
                $file_path = $path.$file.$extension;
                if( file_exists( $file_path ) )
                {
                    include_once( $file_path );
                    break;   
                }		 				 		 		 
            }			 			 

        }); 

    }

    /**
     * Get the config file, include it and set the static config variable 
     */
    private static function setConfigVars()
    {
        $config_file_path = self::getApplicationPath().'config/config.php';
        if( file_exists( $config_file_path ) )
        {
            self::$config = include_once( $config_file_path );
        }
        else 
        {
            throw new UnableToFindConfigException; 
        }
    }

    /**
     * Private function that sets all framework paths for ease of access 
     */
    private static function setPaths()
    {
        self::$application_path    = APPLICATION_PATH; 
        self::$controllers_path    = APPLICATION_PATH.'controllers/';
        self::$system_library_path = SYSTEM_PATH.'libs/'; 
        self::$models_path         = APPLICATION_PATH.'models/';
        self::$system_path         = SYSTEM_PATH;		
        self::$views_path          = APPLICATION_PATH.'views/';			
    }

    private static function setFileExtension()
    {
        self::$file_extension = FILE_EXT; 
    }

}
class UnableToFindClassException extends Exception{}
class UnableToFindConfigException extends Exception{}
