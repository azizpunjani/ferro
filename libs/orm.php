<?php
/**
 * Class ORM abstracts database calls using a simplified interface. 
 * This ORM uses the PDO driver to interact with the database. 
 */
abstract class orm
{    
    protected $table_name; 
    protected $primary_key = 'id'; 
    private $_db; 
    private $_where    = array(); 
    private $_or_where = array(); 
    private $_order_by = array(); 
    private $_object   = array(); 
    private $_query; 
    private $_fetch_as_object          = true;  
    private $_initial_property_values  = array(); 
    private $_dirty_properties         = array(); 
    private $_is_new_record            = true; 
    private $_query_field_values       = array(); 

    #-- Query type constants 
    const SELECT_QUERY = 1; 
    const UPDATE_QUERY = 2;
    const DELETE_QUERY = 3;
    const INSERT_QUERY = 4;  

    #-- Static factory method for convenient instantiation 
    public static function model( $model_name )
    {        
        return new $model_name();  	                
    }

    function __construct()
    {        
        $this->_db = db::getInstance();
        $this->_setTableName( get_class( $this ) );
    } 

    public function __set( $column, $value )
    {	
        #-- If table name is not set then we know these properties are getting 
        #-- set via a database call. 

        if( is_null( $this->table_name ) )
        {
            #-- If properties are getting set via a database call then
            #-- we know this is not a new record. Populate the initial column values incase we need them 
            #-- in the future. 
            $this->_initial_property_values[ $column ] = $value;
            $this->_is_new_record = false;
        }
        else
        {  
            #-- If table name is not null then column is getting modified with a new value 
            #-- mark column as dirty 
            $this->_dirty_properties[ $column ] = $value;
        } 

        #-- Set object's coumn value 
        $this->_object[ $column ] = $value; 
    }

    public function __get( $column )
    {
        #-- Get current objects column value 
        if( isset( $this->_object[ $column ] ) )
        {
            return $this->_object[ $column ];
        }

        #-- Thrown an exception if the column is not found 
        throw new PropertyNotFoundException('The property '.$column.' was not found for model '.$this->table_name ); 
    }

    /**
     * Find one record that matches the query
     * **warning** only use this method if you are absolutely sure 
     * that there is only one record with the specified id otherwise 
     * if there are multiple records, this function will return null. 
     * @param $id - mixed 
     */
    public function findOne( $id )
    {
        return $this->_find( $id ); 
    }

    /**
     * Find all records that match the query 
     * @return array   
     */
    public function findAll()
    {
        #-- Simply a find with null passed in for the id 
        return $this->_find(); 
    }

    /**
     * Get results as array as opposed to object(s)
     * 
     */
    public function asArray()
    {
        $this->_fetch_as_object = false; 
        return $this; 
    }
    /**
     * Order results by specified column(s) 
     * @param array $order_by 
     * Example usage orm::orderBy(array( 'id' => 'DESC' ) ); 
     */   
    public function orderBy( $column, $order )
    {
        $this->_order_by[ $column ] = strtoupper( $order );
        return $this;
    }

    /**
     * Save Model 
     * @return boolean true  - on success 
     *                 false - on faliure 
     */
    public function save()
    {
        #-- Insert the record 
        if( $this->_is_new_record )
        {	
            $this->_constructInsertQuery();
            $this->_prepareQuery(); 
            return $this->_runQuery( orm::INSERT_QUERY ); 
        }
        else 
        {
            #-- Update the record 
            #-- If updating a record with no changes made to it then return true 
            try 
            {
                $this->_constructUpdateQuery();
            }
            catch( NoColumnsToInsertOrUpdateException $exception)  
            {
                return true; 
            }
            $this->_prepareQuery(); 
            return $this->_runQuery( orm::UPDATE_QUERY ); 
        }
    }

    /**
     * Specify which fields to select
     * @param array $fields
     */
    public function select( $fields = array() )
    {
        $this->_select_fields = $fields;  
        return $this; 
    }       

    /**
     * Add OR statements to query 
     * @param string $field
     * @param string $operator
     * @param string $value
     * @return orm
     */
    public function orWhere( $field, $operator, $value )
    {
        $this->_or_where[] = array( 
            'field'    => $field, 
            'operator' => $operator, 
            'value'    => $value 
        );   
        return $this; 
    }

    /**
     * Add where filter to query 
     * @param string $field
     * @param string $operator
     * @param mixed $value
     */
    public function where( $field, $operator, $value )
    {
        $this->_where[] = array( 
            'field'    => $field, 
            'operator' => $operator, 
            'value'    => $value 
        );   
        return $this; 
    }

    /**
     * Internal function to set table_name
     * If table name is set in the model then use that otherwise use the modelname 
     * @param string $model_name
     */
    private function _setTableName( $model_name )
    {
        if( is_null( $this->table_name ) )
        {
            $this->table_name = $model_name;
        }
    }

    /**
     * Construct insert query 
     * @throws CannotInsertBlankObject - thrown when attempt is made to insert a blank object
     */
    private function _constructInsertQuery()
    {
        if( empty( $this->_object ) )
        {
            throw new CannotInsertBlankObject('Error blank object provided for insertion'); 
        }

        $this->_query = ''; 
        $this->_query_field_values = array(); 
        $columns = array_keys( $this->_object );
        $values  = array_values( $this->_object);

        #-- Construct query 
        $query   = 'INSERT INTO '.$this->table_name;
        $query  .= ' ( '.implode(',', $columns).' ) ';

        #-- Fill query with equal number of placeholders as object properties to be inserted
        $query  .= 'VALUES ('.implode(',', array_fill( 0, count($values), '?') ).' ) ';
        $this->_query = $query; 
        $this->_query_field_values = $values;   
    }

    /**
     * Construct select query. 
     */
    private function _constructSelectQuery()
    {
        #-- If select fields were specified then include them as part of the 
        #-- select statement. 
        if( !empty( $this->_select_fields ) )
        {
            $query  = 'SELECT '.implode(',', $this->_select_fields );
            $query .= ' FROM ';     
        }
        #-- Else select all the fields 
        else
        {
            $query = 'SELECT * FROM ';
        }

        $query .= $this->table_name;

        #-- Where 1=1 for convenience when building dynamic queries 
        $query .= ' WHERE 1=1 ';

        #-- If filtered by where clause then construct clause 
        if( !empty( $this->_where ) )
        {
            foreach( $this->_where as $key => $expression )
            {
                $query .= 'AND '.$expression['field'].' '.$expression['operator'].' ? ';
                $this->_query_field_values[] = $expression['value']; 
            }
        }

        #-- If OR clause is set then construct clause
        if( !empty( $this->_or_where ) )
        {
            foreach( $this->_or_where as $key => $expression )
            {
                $query .= 'OR '.$expression['field'].' '.$expression['operator'].' ? ';
                $this->_query_field_values[] = $expression['value'];  
            }
        }

        #-- If order by caluse is set then construct clause  
        if( !empty( $this->_order_by ) )
        {
            $query .= ' ORDER BY '; 
            $order_columns = array();  
            foreach( $this->_order_by as $column => $order )
            {
                $order_columns [] = $column.' '.$order; 
            }
            $query .= implode(', ', $order_columns);
        }

        $this->_query = $query;  
    }

    /**
     * Construct update query
     * @throws NoColumnsToInsertOrUpdateException - thrown when saving an unmodified object  
     */
    private function _constructUpdateQuery()
    {
        if( empty( $this->_dirty_properties ) )
        {
            throw new NoColumnsToInsertOrUpdateException; 
        } 

        $this->_query = '';
        $this->_query_field_values = array();
        $query  = 'UPDATE '.$this->table_name.' SET '; 
        foreach( $this->_dirty_properties as $column => $value )
        {
            $query .= " $column = ? ";
            $this->_query_field_values [] = $value; 
        }  
        $query .= 'WHERE '.$this->primary_key.' =  ?'; 
        $this->_query = $query;
        $this->_query_field_values [] = $this->_initial_property_values[ $this->primary_key ];     

    }

    /**
     * Internal find method. The function finds a record(s) depending on 
     * @param $id - type [mixed] 
     * @return array  - for findAll() 
     *         object - for find
     *         null   - for find if no record is found  
     */
    private function _find( $id = null )
    {
        #-- If id is array then set where clause to each 
        #-- column and value passed in 
        if( is_array( $id ) )
        {
            foreach( $id as $column => $value )
            {
                $this->_where[] = array('field' => $this->primary_key, 
                    'operator' => '=', 
                    'value' => $value 
                );
            }
        } 
        #-- Otherwise if id is not null and not empty then 
        #-- set the where clause with the id passed in 
        else if( !is_null( $id ) && !utility::isEmptyString( $id ) )
        {
            $this->_where[] = array( 'field' => $this->primary_key, 
                'operator' => '=', 
                'value' => $id 
            );
        }

        #-- Construct the query, prepare it and run it
        $this->_constructSelectQuery();
        $this->_prepareQuery();
        $result = $this->_runQuery();

        #-- If id was not passed in to this function i.e findAll()
        #-- then return the result array 
        if( is_null( $id ) )
        {
            return $result;
        }

        #-- At this point we know that find( $id ) was called so we need to return 
        #-- one record if the result contains exactly one record
        if( count( $result ) === 1 )
        {
            return $result[0];
        }

        #-- Otherwise return null 
        return null;
    }

    /**
     * Prepare query by calling the db's prepare function
     */
    private function _prepareQuery()
    {
        return $this->_db->prepare( $this->_query );
    }

    /**
     * Run the query
     * @param unknown_type $query_type
     * @return Returns an array of results or a boolean true or false 
     */
    private function _runQuery( $query_type = orm::SELECT_QUERY )
    {    	      	 
        #-- Execute the prepared statement and pass in the field values 
        if( $this->_db->execute( $this->_query_field_values ) )
        { 
            #-- If fetch as object is set then set the fetch mode as class 
            if( $this->_fetch_as_object )
            {
                $this->_db->statement->setFetchMode( PDO::FETCH_CLASS, $this->table_name ); 
            }
            #-- Otherwise fetch as an associative array 
            else 
            {
                $this->_db->statement->setFetchMode( PDO::FETCH_ASSOC ); 
            }

            #-- Depending on the query choose the correct return value
            switch( $query_type )
            {
            case orm::SELECT_QUERY: 
                return $this->_db->statement->fetchAll();
                break; 
            case orm::INSERT_QUERY:
                if( $this->_db->statement->rowCount() > 0 )
                {
                    return $this->_db->lastInsertId(); 
                }
                else 
                {
                    return false; 
                }
                break; 
            case orm::UPDATE_QUERY: 
            case orm::DELETE_QUERY:
                if( $this->_db->statement->rowCount() > 0 )
                {
                    return true;
                }
                else 
                {
                    return false; 
                } 
                break; 

            } 
        }                       
    }
} 
class PropertyNotFoundException extends Exception{} 
class NoColumnsToInsertOrUpdateException extends Exception{}
class CannotInsertBlankObject extends Exception{}
?>
