<?php
/**
 * Database class. This class uses PDO to perform database functions such
 * as connecting to the database, preparing queries, and executing queries.  
 *
 */
class db
{
    private $_config; 
    private static $_instance; 
    private $_conn;
    public  $statement;  

    /**
     * Private constructor so class intantiation 
     * is done via db::getInstance()  
     */
    private function __construct()
    {
        $config = framework::getConfig(); 
        $this->_config = $config['database']; 
        $this->_connect();                  
    }       

    /**
     * Singleton the database class instance 
     * Usage: $database = db::getInstance(); 
     */
    public static function getInstance()
    {
        if( !self::$_instance )
        {
            self::$_instance = new db(); 
        }

        return self::$_instance; 
    }

    /**
     * Connect to the database 
     * @throws DatabaseException - thrown if unable to connect to the database 
     */
    private function _connect()
    {
        if( $this->_conn ) 
        {
            return;    
        }

        extract( $this->_config );
        try
        {         
            $this->_conn = new PDO( $dsn, $username, $password );
            $this->_conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        catch( PDOException $e )
        {
            throw new DatabaseException('Unable to connect to database '.$e->getMessage() );    
        }   
    }

    /**
     * Execute PDO statement 
     * @param array $fields
     * @throws DatabaseException
     * @return boolean
     */
    public function execute( $fields )
    {
        $this->_conn || $this->_connect();         
        try
        { 
            $success = $this->statement->execute( $fields ); 
        }
        catch( PDOException $e )
        {
            throw new DatabaseException('Error executing sql '.$e->getMessage() ); 
        }
        return $success;         
    }

    /**
     * Prepare query
     * @param string $query
     * @throws DatabaseException - thrown if unable to prepare statement
     */
    public function prepare( $query )
    {
        $this->_conn || $this->_connect();
        try
        {         
            $this->statement = $this->_conn->prepare( $query );                
        }
        catch( PDOException $e )
        {
            throw new DatabaseException('Failed to prepare statement '.$e->getMessage() );  
        }

        #-- If statement is false then prepare failed, raise an exception 
        if( !$this->statement )
        {
            throw new  DatabaseException('Failed to prepare statement '.$this->statement->errorInfo() ); 
        } 
    }

}
class DatabaseException extends Exception{}; 
?>
