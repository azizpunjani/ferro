<?php
/**
 * Abstract controller that can be extended
 */
abstract class controller
{
    public function displayView( $view, $data = array() )
    {
        #-- Extract passed in data as a standalone variable 
        extract( $data );  	

        $view_path = framework::getViewsPath(); 
        $file_ext  = framework::getFileExtension(); 

        $view_file = $view_path.$view.$file_ext; 
        #-- If file exists then include it 
        if( file_exists( $view_file ) )
        {
            include_once( $view_file );  
        }
        else 
        {
            throw new NonExistentViewFile('Unable to find view file for page requested '.$view_file ); 
        }
    }

    /**
     * Display partial view files that are stored in /views/partial_templates/
     * @param string $view
     * @param array $data
     */
    public function displayPartial( $view, $data = array() )
    {
        $view  = 'partial_templates/'.$view; 
        $this->displayView( $view, $data ); 
    }
}

class NonExistentViewFile extends Exception{}
